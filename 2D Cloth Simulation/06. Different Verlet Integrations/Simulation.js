class Simulation{
	constructor(){
		this.points = [];
		this.constraints = [];
		this.gravity = new Vector2(0,150);

		let point0 = new MassPoint(new Vector2(150,100),new Vector2(0,150),1);
		let point1 = new MassPoint(new Vector2(200,200),new Vector2(0,150),1);
		let point2 = new MassPoint(new Vector2(100,200),new Vector2(0,150),1);


		this.points.push(point0);
		this.points.push(point1);
		this.points.push(point2);

		this.constraints.push(new Constraint(point0,point1,1));
		this.constraints.push(new Constraint(point1,point2,1));
		this.constraints.push(new Constraint(point2,point0,1));
	}
	

	getMassPointAtPosition(position){
		for(let i=0;i<this.points.length;i++){
			if(this.points[i].isInside(position)){
				return this.points[i];
			}
		}
		return null;
	}

	update(dt){
        console.log("Simulation update");
		
		for(let i=0;i<this.points.length;i++){
			this.points[i].setGravity(this.gravity);
			this.points[i].update(dt);
		}

		
		for(let i=0;i<this.constraints.length;i++){
			this.constraints[i].update();
		}
	}
	
	
	draw(){
		for(let i=0;i<this.points.length;i++){
			this.points[i].draw();
		}

		for(let i=0;i<this.constraints.length;i++){
			this.constraints[i].draw();
		}
	}	
}