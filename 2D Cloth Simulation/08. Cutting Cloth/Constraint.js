class Constraint{
    constructor(massPointA, massPointB, strength){
        this.massPointA = massPointA;
        this.massPointB = massPointB;
        this.strength = strength;
        this.restLength = Sub(massPointB.pos, massPointA.pos).Length();
        this.shouldBreak = false; // step 2
        this.breakingThreshold = Number.MAX_VALUE; // step 2
    }

    setBreakingThreshold(threshold){
        this.breakingThreshold = threshold;
    }

    update(){
        let direction = Sub(this.massPointB.pos, this.massPointA.pos);
        let currentLength = direction.Length();
        let difference = (this.restLength - currentLength);
        if(Math.abs(difference) >= this.breakingThreshold){ // step 2
            this.shouldBreak = true;
        }


        let correctionPercentage = (difference / currentLength) / 2;

        let offset = Scale(direction, correctionPercentage * this.strength);
        direction.Normalize();


        if(!this.massPointA.isPinned){
            this.massPointA.pos = Sub(this.massPointA.pos, offset);
        }

        if(!this.massPointB.isPinned){
            this.massPointB.pos = Add(this.massPointB.pos, offset);
        }
    }

    draw(){
        DrawUtils.drawLine(this.massPointA.pos, this.massPointB.pos, "black", 1);
    }

}