class Playground{
    constructor(){
        this.simulation = new Simulation();
        this.mousePosition = Vector2.Zero();
        this.selectedPoint = null;


        this.clothResolution = 10;
        this.clothWidth = 1000;
        this.clothHeight = 500;
        this.breakingThreshold = 6;
        this.initialize();
    }

    initialize(){
        // Create cloth
        let massPointsX = parseInt(this.clothWidth / this.clothResolution);
        let massPointsY = parseInt(this.clothHeight / this.clothResolution);
        let massPoints2d = [];

        for(let y = 0; y < massPointsY; y++){
            massPoints2d[y] = [];
            for(let x = 0; x < massPointsX; x++){
                let offset = new Vector2(canvas.width / 2 - this.clothWidth / 2, 50);
                let position = new Vector2(x * this.clothResolution + offset.x,y * this.clothResolution + offset.y);
                massPoints2d[y][x] = this.simulation.createMassPoint(position);

                if(x % 1 == 0 && y == 0 ||  y == 0 && x == massPointsX-1){
                    massPoints2d[y][x].isPinned = true;
                }
            }
        }

        // create structural constraints
        for(let y = 0; y < massPointsY; y++){
            for(let x = 0; x < massPointsX; x++){
                if(x < massPointsX-1){
                    this.simulation.createConstraint(massPoints2d[y][x],massPoints2d[y][x+1]);
                }
                if(y < massPointsY-1){
                    this.simulation.createConstraint(massPoints2d[y][x],massPoints2d[y+1][x]);
                }
            }
        }

        // create shear constraints
        for(let y = 0; y < massPointsY-1; y++){
            for(let x = 0; x < massPointsX-1; x++){
                this.simulation.createConstraint(massPoints2d[y][x],massPoints2d[y+1][x+1],1);
                this.simulation.createConstraint(massPoints2d[y+1][x],massPoints2d[y][x+1],1);
            }
        }

        // create bending constraints
        for(let y = 0; y < massPointsY-2; y++){
            for(let x = 0; x < massPointsX-2; x++){
                // in x direction
                this.simulation.createConstraint(massPoints2d[y][x],massPoints2d[y][x+2],1);
                // in y direction
                this.simulation.createConstraint(massPoints2d[y][x],massPoints2d[y+2][x],1);
            }
        }


        // enable breaking
        this.simulation.enableBreaking(this.breakingThreshold);
        console.log("Mass points created: " + this.simulation.points.length);
    }

    update(dt){
        this.simulation.update(0.02);

        if(this.selectedPoint != null){
            this.selectedPoint.pos = this.mousePosition.Cpy();
            this.selectedPoint.oldPos = this.mousePosition.Cpy();
        }
    }

    draw(){
        this.simulation.draw();
    }



    onMouseMove(position){
        this.mousePosition = position;
    }

    onMouseDown(button){
        let point = this.simulation.getMassPointAtPosition(this.mousePosition);
        console.log(point);
        if(point != null){
            this.selectedPoint = point;
        }
    }

    onMouseHold(button){
        console.log(button);
        if(button == 2){
            let point = this.simulation.getMassPointAtPosition(this.mousePosition);
            if(point != null){
                this.simulation.deleteConstraint(point);
            }
        }
    }

    onMouseUp(button){
        this.selectedPoint = null;
    }

}