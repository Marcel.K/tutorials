class MassPoint{

	constructor(pos, gravity, mass){
		this.pos = pos;
		this.oldPos = pos;
		this.gravity = gravity;
        this.velocity = new Vector2(0,0);	
		
		this.acceleration = new Vector2(0,0);
		this.forceAccumulator = new Vector2(0,0);	
		this.color = "black";
		
		if(mass > 0)this.invMass = 1/mass;
		else this.isPinned = true;
	}
	
	
	update(deltaTime){
		if(!this.isPinned){
			this.addForce(Scale(this.gravity,1/this.invMass));
			this.applyAcceleration();
			this.integrateVerlet(deltaTime);

            if(this.pos.y > canvas.height){
                this.pos.y = canvas.height;
                this.oldPos.y = this.pos.y + this.velocity.y * 1.0;
            }
		}
	}
	
	
	applyAcceleration(){
		// F = m * a
		// a = F / m
		let accelerationOf = Scale(this.forceAccumulator,this.invMass);
		this.acceleration = Add(this.acceleration,accelerationOf);	
	}	

    // Non-constant time differences
	// Standard Verlet Integration
	integrateVerlet(deltaTime){
		// x_n+1 = 2 * x_n - x_n-1 + a * dt^2

		let positionTerm = Sub(Scale(this.pos, 2), this.oldPos);
		let accelerationTerm = Scale(this.acceleration, deltaTime * deltaTime);
		this.velocity = Sub(this.pos, this.oldPos);

		this.oldPos = this.pos;  // Speichern der alten Position
		this.pos = Add(positionTerm, accelerationTerm);

		this.acceleration = new Vector2(0,0);
		this.forceAccumulator = new Vector2(0,0);
	}
	
	addForce(addForce){
		this.forceAccumulator = Add(this.forceAccumulator,addForce);
	}
	
	draw(canvasContext){
        DrawUtils.drawPoint(this.pos, 5, this.color);
	}
}