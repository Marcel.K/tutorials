class Simulation{
	constructor(){
		this.points = [];
		let point = new MassPoint(new Vector2(100,100),new Vector2(0,50),1);
		this.points.push(new MassPoint(new Vector2(100,100),new Vector2(0,10),1));

		this.run = false;
	}
	
	update(dt){
        console.log("Simulation update");
		for(let i=0;i<this.points.length;i++){
			this.points[i].update(0.25);
		}
	}
	
	
	draw(){
		for(let i=0;i<this.points.length;i++){
			this.points[i].draw();
		}
	}	
}