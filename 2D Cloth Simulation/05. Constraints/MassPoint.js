class MassPoint{

	constructor(pos, gravity, mass, radius = 25){
		this.pos = pos;
		this.oldPos = pos;
		this.gravity = Vector2.Zero();
        this.velocity = Vector2.Zero();
		
		this.acceleration = Vector2.Zero();
		this.forceAccumulator = Vector2.Zero();
		this.color = "black";
		this.radius = radius;
		
		if(mass > 0)this.invMass = 1/mass;
		else this.invMass = 0;
	}
	
	
	update(deltaTime){
		if(!this.isPinned){
			this.addForce(Scale(this.gravity,1/this.invMass));
			this.applyAcceleration();
			this.integrateVerlet(deltaTime);

            if(this.pos.y > canvas.height){
                this.pos.y = canvas.height;
                this.oldPos.y = this.pos.y + this.velocity.y * 1.0;
            }
		}
	}
	
	isInside(position){
		let distance = Sub(this.pos,position).Length();
		return distance < this.radius;
	}

	setGravity(gravity){
		this.gravity = gravity;
	}
	
	applyAcceleration(){
		// F = m * a
		// a = F / m
		let accelerationOf = Scale(this.forceAccumulator,this.invMass);
		this.acceleration = Add(this.acceleration,accelerationOf);	
	}	

    // Non-constant time differences
	//enhanced Verlet integration
	integrateVerlet(deltaTime){
		// x1 = x + (x – x0) * (dt / dt0) + a * dt * (dt + dt0) / 2

		let positionTerm = Sub(Scale(this.pos, 2), this.oldPos);
		let accelerationTerm = Scale(this.acceleration, deltaTime * deltaTime);
		this.velocity = Sub(this.pos, this.oldPos);

		this.oldPos = this.pos;  // Speichern der alten Position
		this.pos = Add(positionTerm, accelerationTerm);

		this.acceleration = Vector2.Zero();
		this.forceAccumulator = Vector2.Zero();
	}
	
	addForce(addForce){
		this.forceAccumulator = Add(this.forceAccumulator,addForce);
	}
	
	draw(canvasContext){
        DrawUtils.drawPoint(this.pos, this.radius, this.color);
	}
}