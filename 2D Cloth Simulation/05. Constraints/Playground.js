class Playground{
    constructor(){
        this.simulation = new Simulation();
        this.mousePosition = Vector2.Zero();
        this.selectedPoint = null;
    }


    update(dt){
        this.simulation.update(dt);

        if(this.selectedPoint != null){
            this.selectedPoint.pos = this.mousePosition.Cpy();
            this.selectedPoint.oldPos = this.mousePosition.Cpy();
        }
    }

    draw(){
        this.simulation.draw();
    }



    onMouseMove(position){
        this.mousePosition = position;
    }

    onMouseDown(button){
        let point = this.simulation.getMassPointAtPosition(this.mousePosition);
        if(point != null){
            this.selectedPoint = point;
        }
    }

    onMouseUp(button){
        this.selectedPoint = null;
    }

}