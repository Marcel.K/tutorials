class Playground{
    constructor(){
        this.simulation = new Simulation();
    }


    update(dt){
        this.simulation.update(dt);
    }

    draw(){
        this.simulation.draw();
    }



    onMouseMove(position){
        position.Log();
    }

    onMouseDown(button){
        console.log("Mouse button pressed: " + button);
    }

    onMouseUp(button){
        console.log("Mouse button released: " + button);
    }

}