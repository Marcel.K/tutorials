class Simulation{
	constructor(){
		this.particles = [];
		this.AMOUNT_PARTICLES = 1000;
		this.VELOCITY_DAMPING = 1;
		
		this.instantiateParticles();
	}
	
	// creating a rectangular grid of particles
	instantiateParticles(){
		let offsetBetweenParticles = 10;
		let offsetAllParticles = new Vector2(750,100);
		
		let xparticles = Math.sqrt(this.AMOUNT_PARTICLES);
		let yparticles = xparticles;

		for(let x=0; x<xparticles; x++){
			for(let y=0; y<yparticles; y++){
				let position = new Vector2(x*offsetBetweenParticles + offsetAllParticles.x,
					y*offsetBetweenParticles + offsetAllParticles.y);
				this.particles.push(new Particle(position));
				
				this.particles[this.particles.length-1].velocity = Scale(new Vector2(-0.5 + Math.random(),-0.5 + Math.random()),200);
			}
		}		
	}

	// Algorithm 1 
	update(dt){
		// line 1 - 3
        this.applyGravity();
		// line 6 - 10
		this.predictPositions(dt);
		// line 16
		this.doubleDensityRelaxation();

		// line 18 - 20
		this.computeNextVelocity(dt);

		// make sure particles stay in the world
		this.worldBoundary();
	}



	worldBoundary(){
		for(let i = 0; i < this.particles.length;i++){
			let pos 	= this.particles[i].position;
			let prevPos = this.particles[i].prevPosition;
			


			if(pos.x < 0){
				this.particles[i].velocity.x *=-1;
			}
			
			if(pos.y < 0){
				this.particles[i].velocity.y *=-1;
			}
			
			if(pos.x > canvas.width-1){
				this.particles[i].velocity.x *=-1;
			}
			
			if(pos.y > canvas.height-1){
				this.particles[i].velocity.y *=-1;
			}
		}		
	}

	predictPositions(dt){
		for(let i = 0; i < this.particles.length;i++){
			this.particles[i].prevPosition = this.particles[i].position.Cpy();
			this.particles[i].position = Add(this.particles[i].position, Scale(this.particles[i].velocity,dt * this.VELOCITY_DAMPING));
		}		
	}

	computeNextVelocity(dt){
		for(let i = 0; i < this.particles.length;i++){
			this.particles[i].velocity = Scale(Sub(this.particles[i].position,this.particles[i].prevPosition),1/dt);
		}		
	}

	applyGravity(){
		
	}
	
	doubleDensityRelaxation(){
		
	}
	
	draw(){
		for(let i = 0; i < this.particles.length;i++){
			let pos = this.particles[i].position;
			DrawUtils.drawPoint(pos,5,"#28b0ff");
		}	
	}	
}