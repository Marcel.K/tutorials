class Particle{
	constructor(position){
		this.position = position;
		this.prevPosition = position;
		this.velocity = new Vector2(0,0);
		this.color = "#28b0ff";
	}
}